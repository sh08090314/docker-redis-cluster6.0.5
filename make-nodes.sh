#!/bin/bash
#Redis密码
PASSWORD='1234567890'
#Redis版本
REDIS_IMAGE='redis:6.0.5-buster'
#部署路径，此参数与生成服务配置、启动脚本、停止脚本有关
DATA_PATH='/tmp/redis-cluster'
mkdir ./dist/
echo "version: '3'" > ./dist/docker-compose.yml
echo "services:" >> ./dist/docker-compose.yml
for port in `seq 7002 7007`; do \
  mkdir -p ./dist/data/${port}/conf \
  && PORT=${port} PASSWORD=${PASSWORD} envsubst < ./redis-cluster.tmpl > ./dist/data/${port}/conf/redis.conf \
  && PORT=${port} REDIS_IMAGE=${REDIS_IMAGE} envsubst < ./docker-compose.tmpl >> ./dist/docker-compose.yml \
  && mkdir -p ./dist/data/${port}/data; \
done

# 生成服务配置
DP=${DATA_PATH} envsubst < ./redis-cluster.service.tmpl > ./dist/redis-cluster.service && chmod 0644 ./dist/redis-cluster.service
echo '#!/usr/bin/env sh' > ./dist/startup.sh
echo "cd ${DATA_PATH} && docker-compose up >> redis-cluster.log" >> ./dist/startup.sh
chmod 0755 ./dist/startup.sh

echo '#!/usr/bin/env sh' > ./dist/shutdown.sh
echo "cd ${DATA_PATH} && docker-compose down" >> ./dist/shutdown.sh
chmod 0755 ./dist/shutdown.sh

echo "请把./dist 目录移动到装好Docker服务的CentOS操作系统上的 ${DATA_PATH} ，并使用 root 权限执行以下操作："
echo "添加服务、启用、启动："
echo "----------------------------------------------------------------------------"
echo ""
echo "ln -s ${DATA_PATH}/redis-cluster.service /usr/lib/systemd/system/"
echo "systemctl enable redis-cluster"
echo "systemctl start redis-cluster"
echo ""
echo "----------------------------------------------------------------------------"
echo "然后使用【 redis-cli --cluster create 】手动初始化集群"