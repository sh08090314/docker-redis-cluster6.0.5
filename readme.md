
- redis: 6.0.5
- 操作系统：CentOS 7.8.2003
- docker: 19.03.8
## 华为云环境
```yaml
# 去除已安装的docker
yum remove docker docker-common docker-selinux docker-engine
yum install -y yum-utils device-mapper-persistent-data lvm2
# 下载华为云yum源
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.huaweicloud.com/docker-ce/linux/centos/docker-ce.repo
sed -i 's+download.docker.com+mirrors.huaweicloud.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo
yum makecache fast
# 安装docker 相关
yum install -y docker-ce docker-ce-cli containerd.io docker-compose
# 如遇到的错误
Delta RPMs disabled because /usr/bin/applydeltarpm not installed
# 解决这个问题需要安装deltarpm
yum install deltarpm
```
### 设置内网 docker 阿里云镜像
```yaml
vim /etc/docker/daemon.json 
{
        "registry-mirrors": ["https://zxioty7p.mirror.aliyuncs.com"]
}
```
### 启动 docker
```yaml
systemctl start docker
systemctl enable docker
```

### 生成节点配置
```yaml
./make-nodes.sh
```
### 启动容器
```yaml
cd ./dist/
# 前台运行
docker-compose up
# 或后台运行
docker-compose up -d
```

### 连接某个节点
```yaml
docker exec -it redis-node-7002 bash
```

### 创建集群

```yaml
# 查看有多少个容器
docker ps
# 进入redis-node-7002redis容器
docker exec -it dockercomposerediscluster_redis7_1 bash 
# 在docker 容器内外均可，注意：将以下 127.0.0.1 替换为宿主机网卡IP,1234567890 替换为 make-nodes.sh 中配置的密码
echo yes | redis-cli --cluster create 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.0.1:7005 127.0.0.1:7006 127.0.0.1:7007 --cluster-replicas 1 -a 1234567890
```

## 查看集群节点信息
```yaml
# 容器内
redis-cli -p 7002 -a 1234567890 cluster nodes
# 容器外
docker exec -it ec57e9554784 redis-cli -p 7007 -a zxuk3mqUHFMeZX7exF  cluster nodes
docker exec -it d1eb2ec2f2d8 redis-cli -p 7004 -a 1234567890  cluster nodes
```

## 添加、删除、改变从节点的主节点
```yaml
# 主节点,第一个ip:port 为需要添加的节点ip和端口，第二个ip:port为当前集群中的节点和端口
redis-cli --cluster add-node 127.0.0.1:7014 127.0.0.1:7002 -a 1234567890
# 从节点 \
比如：添加7008节点（slave的添加方法，master为7007）
# 节点ID是主节点的ID
# 127.0.0.1:7008 是新加的从节点
# 127.0.0.1:7007 作为从节点的主节点
redis-cli --cluster add-node --cluster-slave --cluster-master-id e9fb18bec68e625962c2194b40b005613139d0fe 127.0.0.1:7008 127.0.0.1:7007  -a 1234567890

# 删除某个节点,127.0.0.1:7002为随意集群节点，c2403ea0a99fcae8d5c79df1251f468c8ec1654c需删除的节点hash
redis-cli --cluster del-node 127.0.0.1:7002 28889d36bbb44d6e18e579cb8aaa465c09f3e644 -a 1234567890

# 删除后的节点再加入集群，需要先在该节点执行cluster reset，再用add-node进行添加
redis-cli -p 7012 -a 1234567890 cluster reset
redis-cli --cluster add-node --cluster-slave --cluster-master-id e9fb18bec68e625962c2194b40b005613139d0fe 127.0.0.1:7012 127.0.0.1:7003  -a 1234567890

# 改变从节点的master，先进入到需要改变的节点，再设置新的主节点
redis-cli -c -p 7014 -h 127.0.0.1 -a 1234567890
cluster replicate 227ff5419532708d5559347db448d191969458e9  //新master的node id 


## 添加主节点新增加的主节点，是没有slots的，需求重启分配slots，连接到集群的任意一节点来对指定节点指定数量的slot进行迁移到指定的节点。
redis-cli --cluster reshard 127.0.0.1:7002 --cluster-from c2403ea0a99fcae8d5c79df1251f468c8ec1654c --cluster-to 6fc480924f2ed6d9a8b3b36048fbb73c7cabe656 --cluster-slots 2732 --cluster-yes --cluster-timeout 5000 --cluster-pipeline 2732 --cluster-replace -a 1234567890

# 删除主节点有从节点其有solts，先 改变从节点的master，然后再把slosts分配到别的节点
redis-cli --cluster reshard 127.0.0.1:7002 --cluster-from c2403ea0a99fcae8d5c79df1251f468c8ec1654c --cluster-to 6fc480924f2ed6d9a8b3b36048fbb73c7cabe656 --cluster-slots 2732 --cluster-yes --cluster-timeout 5000 --cluster-pipeline 2732 -a 1234567890

## 平衡集群中各个节点的slot数量
redis-cli --cluster rebalance 127.0.0.1:7002 -a 1234567890

## 还可以指定权重分配
redis-cli --cluster rebalance --cluster-weight 117457eab5071954faab5e81c3170600d5192270=5 815da8448f5d5a304df0353ca10d8f9b77016b28=4 56005b9413cbf225783906307a2631109e753f8f=3 --cluster-simulate 127.0.0.1:7002 -a 1234567890

```

## 检查集群
```yaml
# 运行以下命令检查，可以查看到slot分配情况，master不为0就正常
redis-cli --cluster check 127.0.0.1:7002 --cluster-search-multiple-owners -a 1234567890

# 集群信息查看，检查key、slots、从节点个数的分配情况
redis-cli --cluster info 127.0.0.1:7002 -a 1234567890

# 修复集群和槽的重复分配问题
redis-cli --cluster fix 127.0.0.1:7013 --cluster-search-multiple-owners -a 1234567890

```
## 导入集群
```yaml
# 外部Redis实例（9021）导入到集群中的任意一节点。
redis-cli --cluster import 192.168.163.132:6379 --cluster-from 192.168.163.132:9021 --cluster-replace 
# 注意：测试下来发现参数--cluster-replace没有用，如果集群中已经包含了某个key，在导入的时候会失败，不会覆盖，只有清空集群key才能导入。
# 并且发现如果集群设置了密码，也会导入失败，需要设置集群密码为空才能进行导入（call）。通过monitor（9021）的时候发现，在migrate的时候需要密码进行auth认证。 
```

### 不能正常一步到位，就删除掉所有redis容器，再执行后台创建容器
```yaml
docker rm -f $(docker ps -a |awk '{print $1}')
```

# 测试集群 不加-c redis集群报错：(error) MOVED
```yaml
# 容器内
redis-node-7002 redis-cli -p 7002 -a 1234567890 -c
# 容器外
docker exec -it redis-node-7002 redis-cli -p 7002 -a 1234567890 -c
```
